/////////////////////////////
// Intersection types
type Admin = {
    name: string;
    privileges: string[];
};

type Employee = {
    name: string;
    startDate: Date;
}

type ElevatedExployee = Admin & Employee;

const e1: ElevatedExployee = {
    name: 'Tim',
    privileges: ['create-server'],
    startDate: new Date()
};

// Type guards for union types
type UnknownEmployee = Employee | Admin;
function printEmployeeInfo(emp: UnknownEmployee) {
    console.log('Name:', emp.name);
    if ('privileges' in emp) {
        console.log('Privileges:', emp.privileges);
    }
    if ('startDate' in emp) {
        console.log('Start Date:', emp.startDate);
    }
};
printEmployeeInfo(e1);
printEmployeeInfo({name: 'Mara', startDate: new Date()});

/////////////////////////////
// Type guards for Intersection types
type Combinable = string | number;
type Numeric = number | boolean;
type Universal = Combinable & Numeric;

// this is an example of function overload
function add(a: number, b: number): number; // overloading
function add(a: string, b: string): string; // overloading
function add(a: string, b: number): string; // overloading
function add(a: number, b: string): string; // overloading
function add(a: Combinable, b: Combinable) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString + b.toString();
    }
    return a + b;
}

const result = add(1, 5);
// result.split(' '); // ERROR!!!, no split function for numbers
const result2 = add('Tim', 'B');
result2.split(' '); // error if no overloading specified

const result3 = add('Tim', 'B');// "as string" is not necessary if overloading
result3.split(' ');



/////////////////////////////
// type guards for classes - instanceof
class Car {
    drive() {
        console.log('driving...');
    }
};

class Truck {
    drive() {
        console.log('driving a truck...');
    };

    loadCargo(amount: number) {
        console.log('Loading cargo:', amount);
    }
};

type Vehicle = Car | Truck;

const v1 = new Car();
const v2 = new Truck();

function useVehicle(vehicle: Vehicle) {
    vehicle.drive();
    // if ('loadCargo' in vehicle) {
    //     vehicle.loadCargo(1000);
    // }
    if (vehicle instanceof Truck) {
        vehicle.loadCargo(1000);
    }
};

useVehicle(v1);
useVehicle(v2);




/////////////////////////////
// Discriminating unions

interface Bird{
    type: 'bird';
    flyingSpeed: number
}

interface Horse{
    type: 'horse';
    runningSpeed: number;
}

type Animal = Bird | Horse;

function moveAnimal(animal: Animal) {
    // if ('flyingSpeed' in animal) {
    //     console.log('moving with speed:', animal.flyingSpeed);
    // }
    
    // instanceof won't work here since Bird and Horse are interfaces

    let speed;
    switch (animal.type) {
        case 'bird':
            speed = animal.flyingSpeed;
            break;
        
        case 'horse':
            speed = animal.runningSpeed;
            break;
    }
    console.log('Moving at speed:', speed);
};

moveAnimal({ type: 'bird', flyingSpeed: 110 });
moveAnimal({ type: 'horse', runningSpeed: 55 });



/////////////////////////////
// Type casting

const paragraph1 = document.querySelector('p');
const paragraph2 = document.getElementById('message-output');

// HTMLElement casting into HTMLInputElement with 'as'
const userInputElement1 = document.getElementById('user-input') as HTMLInputElement;
userInputElement1.value = 'Hi there!';

// HTMLElement casting into HTMLInputElement with angle brackets
const userInputElement2 = <HTMLInputElement>document.getElementById('user-input');
userInputElement2.value = 'Hi there!';

// one more example
const userInputElement3 = document.getElementById('user-input');
if (userInputElement3) {
    (userInputElement3 as HTMLInputElement).value = 'Hi there!';
}




/////////////////////////////
// Index Properties (Types) - with not defined amount of properties
interface ErrorContainer{ // { email: 'Not a valid email', username: 'Must start with a letter' }
    id: string;
    // id2: number; // ERROR!!! can't be number, must be string

    // this will enable many properties
    [property: string]: string; // property name and the value is a string
};

const errorBag: ErrorContainer = {
    id: '1',
    27: 'this is a valid property',
    // property: 1,// ERROR!!! value can't be of type number
    email: 'Not a valid email',
    username: 'Must start with a capital letter',
};




/////////////////////////////
// Function overloads
// this is an example of function overload
function add2(a: number, b: number): number; // overloading
function add2(a: string, b: string): string; // overloading
function add2(a: Combinable, b: Combinable) {
    if (typeof a === 'string' || typeof b === 'string') {
        return a.toString + b.toString();
    }
    return a + b;
}






/////////////////////////////
// Optional Chaining
const fetchedUserData = {
    id: 'u1',
    name: 'Tim',
    job: {
        title: 'CEO',
        description: 'My own company'
    }
};

// console.log(fetchedUserData.job && fetchedUserData.job.title);
console.log(fetchedUserData?.job?.title);





/////////////////////////////
// Nullish coalescing
const userInput = '';

const storedData1 = userInput || 'Default'; // checks if userInput is null or undefined or empty string
console.log('🚀 ~ file: app.ts ~ line 224 ~ storedData', storedData1);

const storedData2 = userInput ?? 'Default'; // checks if userInput is null or undefined only
console.log('🚀 ~ file: app.ts ~ line 228 ~ storedData2', storedData2);