# Learning TypeScript

### How to create and run TS project
- To initialize a type script project, run: tsc --init
- To run the TS proiject compilation in watch mode: tsc -w
- To run single file compilation in watch mode run: tsc app.ts -w
- To use create-react-app use template - typescript. E.g.: npx create-react-app my-app --template typescript

---

###Topics covered:
1. TS Basic
2. Next-gen JS & TS
3. Classes & interfaces
4. Advanced types - 
5. Generics
6. Utility types - https://www.typescriptlang.org/docs/handbook/utility-types.html
7. Decorators. 
	- class-validator (https://github.com/typestack/class-validator).
	- nestjs
