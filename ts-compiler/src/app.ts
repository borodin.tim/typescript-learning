let username = 'Tim';
console.log(username);

const clickHandler = (message: string) => {
    console.log('Clicked!', message);
};

// comment on a button
// const button = document.querySelector('button')!;
const button = document.querySelector('button');
if (button) {
    button.addEventListener('click', clickHandler.bind(null, 'Hello')
    /* () => {
        console.log('clicked..');
    } */
    );
}