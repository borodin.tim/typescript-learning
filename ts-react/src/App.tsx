import { FC, useState } from 'react';

import './App.css';
import TodoList from './components/TodoList';
import NewTodo from './components/NewTodo';
import { Todo } from './todo.model';

// const TODOS_INITIAL_STATE = [
//     {
//       id: 't1',
//       text: 'Finish the course'
//     },
//     {
//       id: 't2',
//       text: 'Drink water'
//     }
//   ];

const App: FC = () => {
  const [todos, setTodos] = useState<Todo[]>([]);

  const addNewTodoHandler = (text: string) => {
    setTodos(oldTodos => oldTodos.concat(
      {
        id: 't' + (+Math.random().toFixed(6) * 1000000).toString(),
        text
      }
    ));
  };
  const todoDeleteHandler = (todoId: string) => {
    setTodos(oldTodos => oldTodos.filter(todo => todo.id !== todoId));
  };
  
  return (
    <div className="App">
      <NewTodo onAddNewTodo={addNewTodoHandler}/>
      <TodoList onDeleteTodo={todoDeleteHandler} items={todos}/>
    </div>
  );
}

export default App;
