import React, { FC } from 'react'

import './TodoList.css';

interface TodoListProps {
    items: {
        id: string,
        text: string
    }[];
    onDeleteTodo: (todoId: string) => void
}

const TodoList: FC<TodoListProps> = (props) => {
    return (
        <ul className="todo-list">
            {props.items.map(todo =>
                <li key={todo.id} className="todo-item">
                    <div className="todo-data">
                        <div className="todo-text">
                            {todo.text}
                        </div>
                        <div className="todo-id">
                            id: {todo.id}
                        </div>
                    </div>
                    <button
                        className="btn-delete-todo"
                        onClick={props.onDeleteTodo.bind(null, todo.id)}
                    >
                        X
                    </button>
                </li>
            )}
        </ul>
    )
}

export default TodoList
