import { FC, useRef } from 'react';

import './NewTodo.css';

type NewTodoProps = {
    onAddNewTodo: (text: string) => void;
}

const NewTodo: FC<NewTodoProps> = (props) => {
    const todoTextRef = useRef<HTMLInputElement>(null);

    const submitHandler = (event: React.FormEvent) => {
        event.preventDefault();
        const enteredText = todoTextRef.current!.value;
        
        if (enteredText) {
            props.onAddNewTodo(enteredText);
        }

        todoTextRef.current!.value = '';
    };

    return (
        <form onSubmit={submitHandler}>
            <span>
                <label htmlFor="todo-text">Todo text</label>
                <input
                    type="text"
                    id="todo-text"
                    ref={todoTextRef}
                    autoComplete='off'
                />
            </span>
            <button type="submit" className="btn-add-new-todo">Add new Todo</button>
        </form>
    );
};

export default NewTodo;
