type Combinable = number | string;
type ConversionDescriptor = 'as-number' | 'as-text'; // Literal type

function combine(
    input1: Combinable,
    input2: Combinable,
    resultType: ConversionDescriptor // Literal type
) {
    let result;
    if (typeof input1 === 'number' && typeof input2 === 'number' || resultType === 'as-number') {
        result = +input1 + +input2;
    } else {
        result = input1.toString() + input2.toString();
    }
    // if (resultType === 'as-number') {
    //     return parseFloat(result);
    // } else {
    //     return result.toString();
    // }
    return result;
}

console.log(combine(30, 26, 'as-number'));
console.log(combine('Tim', 'Mara', 'as-text'));
console.log(combine('33', '34', 'as-number'));
console.log(combine('Tim', 1111, 'as-text'));
console.log(combine('Tim', 1111, 'as-number'));
// console.log(combine(30, 26, 'as-string')); // ERROR!!!
// console.log(combine('Tim', 'Mara', 'as-test')); // ERROR!!!