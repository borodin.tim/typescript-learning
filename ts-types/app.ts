let userInput: unknown;
let userName: string;


userInput = 5;
userInput = 'Tim';

if (typeof userInput === 'string') {
    userName = userInput; // ERROR!!! Type 'unknown' is not assignable to type 'string'.
}

function generateError(msg: string, code: number): never {
    throw { message: msg, errorCode: code };
}

const result = generateError('An error occured', 500);
console.log('🚀 ~ file: app.ts ~ line 16 ~ result', result);