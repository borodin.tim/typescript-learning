// const person: {
//     name: string;
//     age: number;
//     hobbies: string[];
//     role: [number, string]; // tuple type
// } = {
//     name: 'Tim',
//     age: 30,
//     hobbies: ['Sports', 'Cooking'],
//     // role: [2, 'author'], // arrays of 2 elements
//     role: [2, 'author'], // tuple type
// };

// const ADMIN = 0;
// const READ_ONLY = 1;
// const AUTHOR = 2;

enum Role { ADMIN = 'ADMIN', READ_ONLY = 5, AUTHOR = 200 };

const person = {
    name: 'Tim',
    age: 30,
    hobbies: ['Sports', 'Cooking'],
    // role: [2, 'author'], // arrays of 2 elements
    role: Role.ADMIN, // ENUM type
};

// person.role.push('admin'); // allowed
// person.role[1] = 10; // WRONG!!!
// person.role = [0, 'admin']; // allowed

let favActivities: any[];
favActivities = ['Sports', 7, {name: 'Bob'}];

console.log(person.name);

for (const hobby of person.hobbies){
    console.log(hobby.toUpperCase());
    // console.log(hobby.map()); // ERROR!!!
}

if (person.role === Role.ADMIN) {
    console.log('Person is admin');
    
}