// Decorator function
// function Logger(constructor: Function) {
//     console.log('Logging...');
//     console.log(constructor);
// }

// Decorator factory
function Logger(logString: string) {
    console.log('LOGGER FACTORY');
    return function (constructor: Function) {
        console.log(logString + '(Decorator function)');
        console.log(constructor + '(Decorator function)');
    };
};

function WithTemplate(template: string, hookId: string) {
    console.log('TEMPLATE FACTORY');
    return function <T extends { new(...args: any[]): {name: string}}>(originalConstructor: T) {
        return class extends originalConstructor {
            constructor(...args: any[]) {
                super();
                console.log('Rendering template (Decorator function)');
                const hookElement = document.getElementById(hookId);
                if (hookElement) {
                    hookElement.innerHTML = template;
                    hookElement.querySelector('h1')!.textContent = this.name;
                }
            }
        }
    };
};

@Logger('LOGGER')
@WithTemplate('<h1>My Personal Object</h1>', 'app')
class Person {
    name = 'Tim';

    cosntructor() {
        console.log('Creating Person object...');
    }
}

const person = new Person();
console.log(person);





/////////////////////////////
console.log('/////////////////////////////');

function Log(target: any, propertyName: string | Symbol) { // decorator function - property decorator (no support for return value)
    console.log('');
    console.log('Property decorator (function)');
    console.log('target:', target);
    console.log('propertyName:', propertyName);
}

function Log2(target: any, propertyName: string | Symbol, descriptor: PropertyDescriptor){ // decorator function - accessor decorator (supports return value)
    console.log('');
    console.log('Accessor decorator');
    console.log('target:', target);
    console.log('propertyName:', propertyName);
    console.log('descriptor:', descriptor);
}

function Log3(target: any, methodName: string | Symbol, descriptor: PropertyDescriptor): PropertyDescriptor  { // decorator function - method decorator (supports return value)
    console.log('');
    console.log('Method decorator');
    console.log('target:', target);
    console.log('methodName:', methodName);
    console.log('descriptor:', descriptor);
    return {};
}

function Log4(target: any, methodName: string | Symbol, position: number) { // decorator function - parameter decorator (no support for return value)
    console.log('');
    console.log('Parameter decorator');
    console.log('target:', target);
    console.log('methodName:', methodName);
    console.log('position:', position);
}

class Product{
    @Log
    title: string;
    private _price: number;

    @Log2
    set price(val: number) {
        if(val > 0) {
            this._price = val;
        } else {
            throw new Error('Invalid price - should be positive.');
        }
    }

    constructor(t: string, p: number) {
        this.title = t;
        this._price = p;
    }

    @Log3
    getPriceWithTax(@Log4 tax: number) {
        return this._price * (1 + tax);
    }
}
// const book1 = new Product('book1', 20);
// const book2 = new Product('book2', 40);

function Autobind(_: any, _2:  string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;
    const adjDescriptor: PropertyDescriptor = {
        configurable: true,
        enumerable: false,
        get() {
            const boundFn = originalMethod.bind(this);
            return boundFn;
        }
    };
    return adjDescriptor;
}

class Printer{
    message = 'This works!';

    @Autobind
    showMessage() {
        console.log(this.message);
    }
}

const p = new Printer();

const button = document.querySelector('button')!;
button.addEventListener('click', p.showMessage);





/////////////////////////////
// Decorators for validation

interface ValidatorConfig{
    [property: string]: {
        [validatableProperty: string]: string[] // ['required', 'positive']
    }
}

const registeredValidators: ValidatorConfig = {};

function Required(target: any, propertyName: string) {
    registeredValidators[target.constructor.name] = {
        ...registeredValidators[target.constructor.name],
        [propertyName]: ['required']
    }
}

function PositiveNumber(target: any, propertyName: string) {
    registeredValidators[target.constructor.name] = {
        ...registeredValidators[target.constructor.name],
        [propertyName]: ['positive']
    }
}

function validate(obj: any) {
    const objValidatorConfig = registeredValidators[obj.constructor.name];
    if (!objValidatorConfig) {
        return true
    }
    let isValid = true;
    for (const prop in objValidatorConfig) {
        for (const validator of objValidatorConfig[prop]) {
            switch (validator) {
                case 'required':
                    isValid = isValid && !!obj[prop];
                    break;
                case 'positive':
                    isValid = isValid && obj[prop] > 0;
                    break;
            }
        }
    }
    return isValid;
}

class Course{
    @Required
    title: string;
    @PositiveNumber
    price: number;

    constructor(t: string, p: number) {
        this.title = t;
        this.price = p;
    }
}

const courseForm = document.querySelector('form')!;
courseForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const titleEl = document.getElementById('title') as HTMLInputElement;
    const priceEl = document.getElementById('price') as HTMLInputElement;

    const title = titleEl.value;
    const price = +priceEl.value;

    const createdCourse = new Course(title, price);
    if (!validate(createdCourse)) {
        alert('Invalid input, please try again!');
        return;
    }
    console.log('🚀 ~ file: app.ts ~ line 166 ~ courseForm.addEventListener ~ createCourse', createdCourse);
});