// const names = ['Tim', 'Mara'];
// const arr: Array<number> = [1, 2, 3];

// const promise: Promise<string> = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         // resolve('This is done');
//         resolve('This is done');
//     }, 2000);
// })

// promise.then(data => {
//     data.split(' ');
// })

/////////////////////////////

function merge<T extends object, U extends object>(objA: T, objB: U) {
    return Object.assign(objA, objB);
};

console.log(merge({ name: 'Tim' }, { age: 33 }));

const mergedObj = merge({ name: 'Tim' }, { age: 33 });
console.log('mergedObj', mergedObj);

const mergedObj2 = merge({ name: 'Tim', hobbies: ['bicycling', 'reading books'] }, { age: 33 });
console.log('mergedObj', mergedObj2);

// mergedObj.age; // typescript error without generics
console.log(mergedObj.name); // no error when using generics  -merge<T, U>


/////////////////////////////

interface Lengthy {
    length: number;
};

function countAndDescribe<T extends Lengthy>(element: T): [T, string] {
    let description = 'Got no value,';
    if (element.length == 1) {
        description = 'Got 1 element';
    } else if (element.length > 0) {
        description = 'Got ' + element.length + ' elements';
    }
    return [element, description];
};

console.log(countAndDescribe('Hi there'));
console.log(countAndDescribe(['sports', 'cooking']));
console.log(countAndDescribe([]));
// console.log(countAndDescribe(123)); // ERROR!!! number has no length property



function extractAndConvert<T extends object, U extends keyof T>(obj: T, key: U) {
    return 'Value: ' + obj[key];
};

console.log(extractAndConvert({name: 'Tim'}, 'name'));
// console.log(extractAndConvert({name: 'Tim'}, 'age')); // ERROR!!!, object has no key age.









/////////////////////////////
// Classes

class DataStorage<T extends string | number | boolean> {
    private data: T[] = [];

    addItem(item: T) {
        this.data.push(item);
    }

    removeItem(item: T) {
        if (this.data.indexOf(item) === -1) {
            return;
        } else {
            this.data.splice(this.data.indexOf(item), 1);
        }
    }

    getItems() {
        return [...this.data];
    }
}

const textStorage = new DataStorage<string>();
textStorage.addItem('Tim');
textStorage.addItem('Mara');
textStorage.removeItem('Mara');
console.log(textStorage.getItems());


const numbersStorage = new DataStorage<number>();
numbersStorage.addItem(1);
numbersStorage.addItem(2);
numbersStorage.removeItem(1);
console.log(numbersStorage.getItems());

// requires more specialized data storage than just primitive types.
// const objStorage = new DataStorage<object>();
// const timObj = { name: 'Tim' };
// objStorage.addItem(timObj);
// objStorage.addItem({ name: 'Harley' });
// objStorage.addItem({ name: 'Mara' });
// // objStorage.removeItem({ name: 'Tim' }); // ERROR!!! indexof will return -1, hence last array elem will be removed
// objStorage.removeItem(timObj);
// console.log(objStorage.getItems());



/////////////////////////////
// Partial

interface CourseGoal{
    title: string;
    description: string;
    completeUntil: Date;
}

function createCourseGoal(title: string, description: string, date: Date): CourseGoal {
    let courseGoal: Partial<CourseGoal> = {};
    
    courseGoal.title = title;
    courseGoal.description = description;
    courseGoal.completeUntil = date;
    
    // return { title, description, completeUntil: date };
    
    return courseGoal as CourseGoal;
}







/////////////////////////////
//  Readonly

const names: Readonly<string[]> = ['Tim', 'Mara'];

// ERRORS
// names.push('Harley');
// name.pop();
