import 'reflect-metadata';
import { plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';

import { Product } from './product.model';

// import _ from 'lodash';
// console.log(_.shuffle([1, 2, 3]));
// // declare var GLOBAL: any;
// // console.log(GLOBAL);
//const p1 = new Product('A Book', 12.99);

const products = [
    { title: 'A Carpet', price: 29.99 },
    { title: 'A book', price: 10.99 }
];

const newProd = new Product('', -5.99);
validate(newProd).then(errors => {
    if (errors.length > 0) {
        console.log('Validation errors: ', errors);
    } else {
        console.log('🚀 ~ file: app.ts ~ line 18 ~ newProd', newProd.getInfo());
    }
});

// #rd party library
const loadedProducts = plainToInstance(Product, products);

// Manual approach
// const loadedProducts = products.map(prod => {
//     return new Product(prod.title, prod.price);
// });

for (const prod of loadedProducts) {
    console.log(prod.getInfo());
};