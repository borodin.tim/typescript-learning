abstract class Department {
    // private id: string;
    // private name: string;
    static fiscalYear = 2021;
    protected employees: string[] = [];

    constructor(protected readonly id: string, public name: string) {
        // this.id = id; // no need
        // this.name = n; // no need
    }

    static createExployee(name: string) {
        return { name: name };
    }

    abstract describe(this: Department): void;
    //{
    //  console.log(`Department: ${this.id}: ${this.name}`);
    //}

    addEmployee(employee: string) {
        this.employees.push(employee);
        // this.id = 'd2'; // ERROR!!! id is readonly
    }

    printEmployeesInformation() {
        console.log('Exmployees amount:', this.employees.length);
        console.log('Exmployees list:', this.employees);
    }
}

class ITDepartment extends Department {
    admins: string[];

    constructor(id: string, admins: string[]) {
        super(id, 'ITDep');
        this.admins = admins;
    }

    describe(): void {
        console.log('IT Department ID:', this.id);
    }
}

class AccountingDepartment extends Department{
    private lastReport: string;
    private static instance: AccountingDepartment;

    private constructor(id: string, private reports: string[]) {
        super(id, 'AccountingDept');
        this.lastReport = reports[0];
    }

    static getInstance() {
        if (this.instance) {
            return this.instance;
        } 
        this.instance = new AccountingDepartment('d2', []);
        return this.instance;
    }

    get mostRecentReport() {
        if (this.lastReport) {
            return this.lastReport;
        } else {
            throw new Error('No last report found!');
        }
    }

    set mostRecentReport(report: string) {
        if (!report) {
            throw new Error('Please pass in a valid report');
        }
        this.addReport(report);
    }

    describe() {
        console.log('Accounting department ID:', this.id);
    }
    
    addEmployee(name: string) {
        if (name === 'Tim') {
            return;
        }
        this.employees.push(name); // sine employees is protected
    }

    addReport(report: string) {
        this.reports.push(report);
        this.lastReport = report;
    }

    printReports() {
        console.log(this.reports);
    }
}

////////////////////////////////////////////////////////////////////

const employee1 = Department.createExployee('John');
// console.log('🚀 ~ file: app.ts ~ line 82 ~ employee1', employee1);
// console.log('Fiscal year:', Department.fiscalYear);


const itDept = new ITDepartment('d1', ['Tim']);
// console.log(itDept);
itDept.describe();
itDept.addEmployee('Tim');
itDept.addEmployee('Mara');
// itDept.employees[1] = 'Harley'; // Error!!! exployees is private
itDept.printEmployeesInformation();
itDept.describe();
// console.log('🚀 ~ file: app.ts ~ line 41 ~ itDept', itDept);


// const accounting = new AccountingDepartment('d2', []);
const accounting = AccountingDepartment.getInstance();
console.log('🚀 ~ file: app.ts ~ line 118 ~ accounting', accounting);
const accounting2 = AccountingDepartment.getInstance();
console.log('🚀 ~ file: app.ts ~ line 118 ~ accounting2', accounting2);
accounting.mostRecentReport = 'First report';
// accounting.addReport('Something Went Wrong');
// console.log('accouting.getLastReport:', accounting.mostRecentReport);
accounting.addReport('Report 1');
// console.log('accouting.getLastReport:', accounting.mostRecentReport);
accounting.printReports();
accounting.addEmployee('Tim');
accounting.addEmployee('Bob');
// accounting.printEmployeesInformation();
accounting.describe();