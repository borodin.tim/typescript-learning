// type AddFn = (a: number, b: number) => number;
interface AddFn  {
    (a: number, b: number): number;
}

let add: AddFn;
add = (n1: number, n2: number) => {
    return n1 + n2;
};

interface Named {
    readonly name?: string;
    outputName?: string;
}

interface AnotherInterface{

}

interface Greetable extends Named, AnotherInterface {
    greet(phrase: string): void;
}

class Person implements Greetable{
    name?: string;
    age = 33;

    constructor(n?: string) {
    // constructor(n: string = 'DefaultName') {
        if (n) {
            this.name = n;
        }
    }

    greet(phrase: string): void {
        if (this.name) {
            console.log(phrase + ' ' + this.name);
        } else {
            console.log('Hi, anonymous');
        }
    }
}

let user1: Greetable;

user1 = new Person();

user1.greet('Hi there, I\'m');
console.log('🚀 ~ file: app.ts ~ line 23 ~ user1', user1);