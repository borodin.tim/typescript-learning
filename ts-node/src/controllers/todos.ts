import { RequestHandler } from "express";

import { Todo } from '../models/todo';

const TODOS: Todo[] = [];

export const createTodo: RequestHandler = (req, res, next) => {
    const text = (req.body as {text: string}).text;
    const newTodo = new Todo(Math.random().toString(), text);
    TODOS.push(newTodo);
    res.status(201).json({message: 'Created the Todo', createdTodo: newTodo})
};

export const getTodos: RequestHandler = (req, res, next) => {
    res.status(200).json({ todos: TODOS });
};

export const updateTodo: RequestHandler<{id: string}> = (req, res, next) => {
    const id = req.params.id;
    const updatedText = (req.body as {text: string}).text;
    const todoIdx = TODOS.findIndex(todo => todo.id === id);
    
    if (todoIdx < 0) {
        throw new Error('Cound not find Todo');
    }

    TODOS[todoIdx] = new Todo(TODOS[todoIdx].id, updatedText);

    res.status(200).json({message: 'Updated!', udpatedTodo: TODOS[todoIdx]});
};

export const deleteTodo: RequestHandler = (req, res, next) => {
    const id = req.params.id;
    const todoIdx = TODOS.findIndex(todo => todo.id === id);

    if (todoIdx < 0) {
        throw new Error('Cound not find Todo');
    }

    TODOS.splice(todoIdx, 1);

    res.status(200).json({ message: "Deleted!", todos: TODOS });
};